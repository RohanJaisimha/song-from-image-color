import json
import re

# goes through a dictionary that contains {"Song Name", "Artist Name", "Link", "Genre"}
# and finds all color names in "Song Name" and stores in dictionary as "Colors"
# which in and of itself is a dictionary with Name and RGB values
def getColors(dict1):
    song_name = dict1["Song Name"].replace(
        "Snow White and the Seven Dwarfs", "")
    song_name = re.sub(" \(feat[\D]*\)", "", song_name)
    song_name = re.sub(" \[feat[\D]*\]", "", song_name)
    song_name = re.sub(" feat[\D]*", "", song_name)
    song_name = song_name.lower()
    song_name = re.sub("[^a-z ]", "", song_name)
    dict1["Colors"] = []
    fin = open("colors_database.json", "r")
    data = json.load(fin)
    fin.close()
    for i in data:
        if(" "+i["name"]+" " in " "+song_name+" "):
            dict1["Colors"].append(i)


def jsonify(f_name):
    fin = open(f_name, 'rb')
    songs = []
    count = 0
    flag = True
    for line in fin:
        if(flag):  # essentially skip the first line
            flag = not flag
            continue
        line = "".join([chr(x) for x in line])
        dict1 = {}
        try:
            dict1["Genre"], dict1["Artist Name"], dict1["Song Name"], dict1["Link"] = line.strip(
            ).split("\t")
            dict1["Link"] = "https://open.spotify.com/track/" + dict1["Link"]
        except ValueError:
            continue
        getColors(dict1)
        if(len(dict1["Colors"]) != 0):
            songs.append(dict1)
            count += 1
            if(count % 1000 == 0):
                print("Added", count, "songs...")

    fin.close()
    fout = open("music_database.json", 'w')
    json.dump(songs, fout)
    fout.close()


def main():
    # dataset: https://www.kaggle.com/zaheenhamidani/ultimate-spotify-tracks-db/
    jsonify("Spotify_Songs.txt")


if(__name__ == "__main__"):
    main()
