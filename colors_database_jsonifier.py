import requests
from bs4 import BeautifulSoup
import json
import re

colors = []

# Takes in a hexadecimal code for a color
# and converts to RGB values
# Eg. convertHexadecimalCodeToRGB("#DABDAB") = (218, 189, 171)
def convertHexadecimalCodeToRGB(text):
    r = int(text[1:3], 16)
    g = int(text[3:5], 16)
    b = int(text[5:], 16)
    return r, g, b


# goes through the url and creates a dictionary with name and rgb information
# then stores dictionary into 'colors,' which is a global list
def pullInfoAndClean(url):
    text = BeautifulSoup(requests.get(url).text, features="html.parser")
    text = text.find(id="mw-content-text").text
    text = text.split("\n")
    text = text[text.index("Value (HSV)")+1:text.index("See also[edit]")]
    text = [i for i in text if(len(i) != 0)]
    for i in range(0, len(text), 10):
        dict1 = {}
        text[i] = text[i].lower() if(
            "(" not in text[i]) else text[i][:text[i].index("(")].strip().lower()
        flag = False
        global colors
        for j in colors:
            if(text[i] == j["name"]):
                flag = True
                break
        if(not flag):
            dict1["name"] = text[i]
            dict1["r"], dict1["g"], dict1["b"] = convertHexadecimalCodeToRGB(
                text[i+1])
            colors.append(dict1)


def main():
    print("Pulling A-F...")
    pullInfoAndClean(
        "https://en.wikipedia.org/wiki/List_of_colors:_A%E2%80%93F")
    print("Pulling G-M...")
    pullInfoAndClean(
        "https://en.wikipedia.org/wiki/List_of_colors:_G%E2%80%93M")
    print("Pulling N-Z...")
    pullInfoAndClean(
        "https://en.wikipedia.org/wiki/List_of_colors:_N%E2%80%93Z")
    print("JSONifying data...")
    fout = open("colors_database.json", 'w')
    json.dump(colors, fout)
    fout.close()
    print("Finished.")


if(__name__ == "__main__"):
    main()
