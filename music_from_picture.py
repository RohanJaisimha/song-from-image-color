from PIL import Image
import random
import json
import sys


# gets the most dominant color in an image
def getMostDominantColor(f_name):
    im = Image.open(f_name)
    most_dominant_color = max(im.getcolors(im.size[0]*im.size[1]))[1]
    im.close()
    return most_dominant_color


def getSongs(f_name):
    fin = open(f_name, 'r')
    songs = json.load(fin)
    fin.close()
    return songs


# uses distance formula to find the distane between two colors
# in terms of RGB values
def distance(l1, l2):
    return sum([(i-j)**2 for i, j in zip(l1, l2)])**0.5


# returns the closest song match to color, which is an array of [r,g,b]
def getMatch(songs, color):
    tol = 1e-3
    l = []
    for song_object in songs:
        temp = []
        for j in song_object["Colors"]:
            temp.append(distance([j['r'], j['g'], j['b']], color))

    # if a song's name has two or more colors in it, we set the distance to the smallest distance
        song_object["Distance"] = min(temp)
        l.append(song_object)

    min_distance = sys.maxsize
    for song_object in songs:
        min_distance = min(min_distance, song_object["Distance"])

    l = [i for i in l if(abs(i["Distance"]-min_distance) < tol)]

    # randomly choose one from the list of matches
    song = random.choice(l)
    for i in song["Colors"]:
        if(abs(distance([i['r'], i['g'], i['b']], color) - min_distance) < tol):
            break
    print("Most dominant color in the image had (R, G, B) value of " +
          str(tuple(color)) + ".")
    print("Closest match was " + i["name"] + ".")
    print("Chose to play " + song["Song Name"] +
          " by " + song["Artist Name"]+".")
    print("Genre: " + song["Genre"])
    print("Spotify Link: " + song["Link"])


def main():
    image_name = input("Enter your image's path here: ")
    json_name = "music_database.json"
    getMatch(getSongs(json_name), getMostDominantColor(image_name))


if(__name__ == "__main__"):
    main()
