# Song from Image Color
* Programs that selects a song based off of the most dominant color in an image
* Dataset for songs: https://www.kaggle.com/zaheenhamidani/ultimate-spotify-tracks-db/
* Dataset for colors: scraped from Wikipedia

# Files
* "Spotify_Songs.txt" which is tab-delimited text file that contains all songs on Spotify
* "colors\_database\_jsonifier.py" which is a python program that creates "colors\_database.json" from Wikipedia using Beautiful Soup
* "music\_database\_jsonifier.py" which is python program that creates "music\_database.json" It goes through Spotify_Songs.txt and filters out all the songs whose names don't have coors in them
* "music\_from\_picture.py" which is the main program that takes in an image, finds the most dominant color, and finds a song with that color's name in it.

# How to Run
* Download/clone the folder onto your device. Navigate to the folder on your command line.
* Install the required modules. This can be done by typing `pip install requirements.txt`
* The json files will already be up to date, but if you want to update them, execute music\_database\_jsonifier.py and colors\_database\_jsonifier.py by typing `python3 music_database_jsonifier.py`. After about 20 minutes, emusic\_database.json will be up to date. Similarly, type `python3 colors_database_jsonifier.py`. This will only take ten seconds to run
* Finally, run music\_from\_picture.py (`python3 music_from_picture.py`), type in the path to your image when prompted and the program will crunch the numbers and print out the song.
* Note: if you get an error, it might be because your image is a .jpg. To fix this, type in `mv [Image_Name].jpg [Image_Name].png` and then run the program again.